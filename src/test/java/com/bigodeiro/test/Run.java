package com.bigodeiro.test;

import com.bigodeiro.model.dao.CustomerDAO;
import com.bigodeiro.model.dao.WSUserDAO;
import com.bigodeiro.model.entity.Customer;
import com.bigodeiro.model.entity.WSUser;
import com.bigodeiro.util.EncryptionUtil;

public class Run {

    public static void main(String[] args) {
        /*WSUser usr = new WSUser();
        usr.setUsername("bigodeiro");
        usr.setPassword(EncryptionUtil.sha256("bigodeiro"));
        usr.setEnable(true);
        WSUserDAO dao = new WSUserDAO();
        dao.insert(usr);*/
        
        Customer ct1 = new Customer();
        ct1.setName("Sr. Barba");
        Customer ct2 = new Customer();
        ct2.setName("Barbearia Avenida");
        CustomerDAO dao = new CustomerDAO();
        dao.insert(ct1);
        dao.insert(ct2);
        System.exit(0);
    }
    
}
