package com.bigodeiro.model.service;

import com.bigodeiro.model.dao.CustomerDAO;
import com.bigodeiro.model.entity.Customer;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/customer")
@RequestScoped
public class CustomerSRV {
    
    private @Inject CustomerDAO dao;
    
    public void add(Customer obj) {
        dao.insert(obj);    
    }
    
    public void set(Customer obj) {
        dao.update(obj);
    }
    
    public void del(Customer obj) {
        dao.delete(obj);
    }
    
    public List<Customer> list() {
        return dao.select(Customer.class);
    }
    
    @GET
    @Path("/list_enable")
    @Produces({MediaType.APPLICATION_XML})
    public List<Customer> listEnable() {
        System.out.println("com.bigodeiro.model.service.CustomerSRV.listEnable()");
        List<Customer> aux = dao.select(Customer.class);
        for (Customer customer : aux) {
            System.err.println(customer.toString());
        }
        return aux;
    }
    
    public Customer get(String name) {
        return dao.select(name);
    }
    
}
