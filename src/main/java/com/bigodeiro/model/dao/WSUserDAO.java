package com.bigodeiro.model.dao;

import com.bigodeiro.model.entity.WSUser;
import com.bigodeiro.util.HibernateUtil;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

@RequestScoped
public class WSUserDAO extends SuperDAO {
    
    private Session session;
    
    public WSUser select(String userName) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.getTransaction().begin();
            Criteria c = session.createCriteria(WSUser.class);
            c.add(Restrictions.eq("username", userName));
            c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            WSUser result = (WSUser) c.uniqueResult();
            return result;
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    
    public List<WSUser> selectAtivos() {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.getTransaction().begin();
            Criteria c = session.createCriteria(WSUser.class);
            c.add(Restrictions.eq("ativo", true));
            c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            List<WSUser> result = (List<WSUser>) c.list();
            return result;
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    
}
