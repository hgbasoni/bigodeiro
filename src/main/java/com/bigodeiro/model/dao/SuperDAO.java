package com.bigodeiro.model.dao;

import com.bigodeiro.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;

@Dependent
public class SuperDAO implements Serializable {
    
    private Session session;
    
    public void insert(Object obj) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.getTransaction().begin();
            session.clear();
            session.save(obj);
            session.flush();
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO: " + ex.toString(), ex.toString()));
        } finally {
            session.close();
        }
    }
    
    public void update(Object obj) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.clear();
            session.update(obj);
            session.flush();
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO: " + ex.toString(), ex.toString()));
        } finally {
            session.close();
        }
    }
    
    public void delete(Object obj) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.clear();
            session.delete(obj);
            session.flush();
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO: " + ex.toString(), ex.toString()));
        } finally {
            session.close();
        }    
    }
    
    public List select(Class clazz) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.getTransaction().begin();
            session.clear();
            List objts = null;
            Criteria selectAll = session.createCriteria(clazz);
            objts = selectAll.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            session.flush();
            session.getTransaction().commit();
            return objts;
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO: " + ex.toString(), ex.toString()));
        } finally {
            session.close();
        }
        return null;
    }
        
}
