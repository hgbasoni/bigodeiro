package com.bigodeiro.model.dao;

import com.bigodeiro.model.entity.Customer;
import com.bigodeiro.util.HibernateUtil;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

@RequestScoped
public class CustomerDAO extends SuperDAO {
    
    private Session session;
    
    public Customer select(String name) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.getTransaction().begin();
            Criteria c = session.createCriteria(Customer.class);
            c.add(Restrictions.eq("name", name));
            c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            Customer result = (Customer) c.uniqueResult();
            return result;
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    
    public List<Customer> selectEnable() {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.getTransaction().begin();
            Criteria c = session.createCriteria(Customer.class);
            c.add(Restrictions.eq("enable", true));
            c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            List<Customer> result = (List<Customer>) c.list();
            return result;
        } catch (HibernateException ex) {
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    
}
