package com.bigodeiro.security;

import com.bigodeiro.model.dao.WSUserDAO;
import com.bigodeiro.model.entity.WSUser;
import com.bigodeiro.util.EncryptionUtil;
import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import org.glassfish.jersey.internal.util.Base64;

@Provider
public class SecurityFilter implements ContainerRequestFilter{
    
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String AUTHORIZATION_PREFIX = "Basic ";
    private static final String SECURED_URL_PREFIX = "secured";

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        //if(requestContext.getUriInfo().getPath().contains(SECURED_URL_PREFIX)) {
            List<String> authHeader = requestContext.getHeaders().get(AUTHORIZATION_HEADER);
            if(authHeader != null && authHeader.size() > 0) {
                String authToken = authHeader.get(0);
                authToken = authToken.replaceAll(AUTHORIZATION_PREFIX, "");
                String decodedString = Base64.decodeAsString(authToken);
                StringTokenizer tokenizer = new StringTokenizer(decodedString, ":");
                WSUser user = new WSUser();
                user.setUsername(tokenizer.nextToken().toLowerCase());
                user.setPassword(tokenizer.nextToken());
                
                WSUser loginUser = new WSUserDAO().select(user.getUsername());
                /*System.out.println("Usuário: " + loginUser.getUsuario() + " | Senha: " + loginUser.getSenha());
                System.out.println("Usuário: " + user.getUsuario() + " | Senha: " + user.getSenha());
                System.out.println("Usuário: " + user.getUsuario() + " | Senha: " + user.getSenha());*/
                if(loginUser != null) {
                    if(EncryptionUtil.sha256(user.getPassword()).equals(loginUser.getPassword())) {
                        return;
                    }
                }
            }
            Response unauthorizedStatus = Response.status(Response.Status.UNAUTHORIZED).entity("User cannot access the resource.").build();
            requestContext.abortWith(unauthorizedStatus);
        //}    
    }
    
}
